# Ice.com Challenge

I'm going to start keeping a record of the code challenges that
prospective employers ask me to do.  Just for the fun of it all.

# Requirements

Read in a series of numbers from a file.  Output the largest sum of
consecutive numbers in the file.  For example, for the following
input:

7 2 -12 2 3 -1 4 7 -3 2 -5

The output of the program should be 15 because 2 + 3 + -1 +4 + 7 = 15.
