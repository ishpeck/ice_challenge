#!env python
from os import path
from sys import argv
from sys import exit
from sys import stderr
from sys import stdout

# *Challenge:*
# Read in a series of numbers from a file.  Output the largest sum of
# consecutive numbers in the file.  For example, for the following
# input:
#
# 7 2 -12 2 3 -1 4 7 -3 2 -5
#
# The output of the program should be 15 because 2 + 3 + -1 +4 + 7 = 15.

def _biggest_sum(numSeq, topSum, lastIdx):
    def _ts(accumSum, idxOffset):
        return max(accumSum, sum(numSeq[:idxOffset]))
    if not numSeq:
        return topSum
    return _biggest_sum(numSeq[1:], 
                        reduce(_ts, range(lastIdx, -1, -1), topSum), 
                        lastIdx-1)

def print_biggest_sum(numSeq):
    stdout.write("{}\n".format(
        _biggest_sum(numSeq, sum(numSeq), len(numSeq))
    ))
    return 0

# Below is all the plumbing

def bad_file_error(resultPassThrough):
    stderr.write("You must specify a file path.\n")
    return resultPassThrough

def as_number(textualNumber):
    try:
        return int(textualNumber)
    except:
        stderr.write("Couldn't convert {} to integer.\n".format(textualNumber))
        exit(2)

def read_input_file(filePath):
    with open(filePath, "r") as inputFile:
        return list(map(as_number, inputFile.read().split()))
    return bad_file_error([])

def main(proggy, filePath=None, *args):
    if filePath is None or not path.exists(filePath):
        return bad_file_error(1)
    return print_biggest_sum(read_input_file(filePath))

if __name__=="__main__":
    exit(main(*argv))
